# Uso de SVC para ver el proceso de entrada y salida de interrupciones.

La finalidad de este ejemplo es ver como se produce el  "[stacking](https://developer.arm.com/documentation/ddi0337/e/Exceptions/Pre-emption/Stacking?lang=en)" cuando se genera una excepción. 
